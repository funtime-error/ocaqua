# OC Aqua Supply

## Landing Page

![Landing Page](demo/landing.png)

---

## Navigation Menu

### Empty Cart

![Nav menu with empty cart](demo/menu-cart-empty.png)

### Full Cart

![Nav menu with full cart](demo/menu-cart-full.png)

---

## Products

### Live Products

![Live products page](demo/live-products.png)

### Scape Products

![Scape products page](demo/scape-products.png)

### All Products

![All products catalog page](demo/all-products.png)

---

## Product Detail

![Product detail page](demo/product-detail.png)

---

## Checkout view

![Checkout page view](demo/checkout.png)

---

## Mobile Responsive

![Mobile-responsive](demo/breakpoints.png)

---

## Search

![Search](demo/search.png)

---
