'use strict';

const stripe = require('stripe')('sk_test_vqMwPpPzr6PWtg4FMy9daliZ00Qbt1sEOS');

/**
 * Order.js controller
 *
 * @description: A set of functions called "actions" for managing `Order`.
 */

module.exports = {
  /**
   * Retrieve order records.
   *
   * @return {Object|Array}
   */

  find: async ctx => {
    if (ctx.query._q) {
      return strapi.services.order.search(ctx.query);
    } else {
      return strapi.services.order.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a order record.
   *
   * @return {Object}
   */

  findOne: async ctx => {
    return strapi.services.order.fetch(ctx.params);
  },

  /**
   * Count order records.
   *
   * @return {Number}
   */

  count: async ctx => {
    return strapi.services.order.count(ctx.query);
  },

  /**
   * Create a/an order record.
   *
   * @return {Object}
   */

  create: async ctx => {
    const {
      address,
      amount,
      products,
      postalCode,
      customerName,
      phoneNumber,
      confirmationEmail,
      token,
      city
    } = ctx.request.body;

    try {
      // TODO: Create Stripe customer to keep track of orders per user

      const charge = await stripe.charges.create({
        amount: parseInt(amount * 100),
        currency: 'usd',
        description: `Order ${new Date(Date.now())} by ${
          customerName
        }`,
        source: token
      });

      const order = await strapi.services.orders.add({
        // user: ctx.state.user.id,
        address,
        amount,
        products,
        postalCode,
        customerName,
        phoneNumber,
        confirmationEmail,
        city
      });

      return order;
    } catch (error) {
      console.error(error.message);
    }
  },

  /**
   * Update a/an order record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.order.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an order record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.order.remove(ctx.params);
  }
};
