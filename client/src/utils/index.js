const CART_KEY = 'cart';
const TOKEN_KEY = 'jwt';
const USERNAME_KEY = 'username';

export const BREAKPOINT_WIDTH_ENUM = Object.freeze({
  DESKTOP_LARGE: 1200,
  DESKTOP_SMALL: 992,
  MOBILE_LARGE: 576,
  TABLET: 768
});

export const BREAKPOINT_TYPE_ENUM = Object.freeze({
  DESKTOP_LARGE: 'DESKTOP_LARGE',
  DESKTOP_SMALL: 'DESKTOP_SMALL',
  MOBILE_LARGE: 'MOBILE_LARGE',
  MOBILE_SMALL: 'MOBILE_SMALL',
  TABLET: 'TABLET'
});

/** CART */

export const CART_ENUM = Object.freeze({
  SALES_TAX: 0.075,
  SHIPPING: 8.63
});

export const calculateCartItemsSubtotalPrice = items => {
  return items.reduce((acc, item) => acc + item.quantity * item.price, 0);
};

export const calculateCartItemsTotalPrice = subtotal => {
  return subtotal + CART_ENUM.SHIPPING + CART_ENUM.SALES_TAX;
};

export const setCart = (value, cartKey = CART_KEY) => {
  if (localStorage) {
    localStorage.setItem(cartKey, JSON.stringify(value));
  }
};

export const getCart = (cartKey = CART_KEY) => {
  if (localStorage && localStorage.getItem(cartKey)) {
    return JSON.parse(localStorage.getItem(cartKey));
  }

  return [];
};

export const clearCart = (cartKey = CART_KEY) => {
  if (localStorage) {
    localStorage.removeItem(cartKey);
  }
};

/** JWT */

export const getToken = (tokenKey = TOKEN_KEY) => {
  if (localStorage && localStorage.getItem(tokenKey)) {
    return JSON.parse(localStorage.getItem(tokenKey));
  }
  return null;
};

export const setToken = (value, tokenKey = TOKEN_KEY) => {
  if (localStorage) {
    localStorage.setItem(tokenKey, JSON.stringify(value));
  }
};

export const clearToken = (tokenKey = TOKEN_KEY) => {
  if (localStorage) {
    localStorage.removeItem(tokenKey);
  }
};

export const getUsername = (usernameKey = USERNAME_KEY) => {
  if (localStorage && localStorage.getItem(usernameKey)) {
    return JSON.parse(localStorage.getItem(usernameKey));
  }
  return null;
};

export const setUsername = (value, usernameKey = USERNAME_KEY) => {
  if (localStorage) {
    localStorage.setItem(usernameKey, JSON.stringify(value));
  }
};

export const clearUsername = (usernameKey = USERNAME_KEY) => {
  if (localStorage) {
    localStorage.removeItem(usernameKey);
  }
};
