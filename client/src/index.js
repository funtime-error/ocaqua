import React from 'react';
import ReactDOM from 'react-dom';

import { StoreProvider } from './store/store';

import * as serviceWorker from './serviceWorker';

import App from './components/App';

const Root = () => (
  <StoreProvider>
    <App />
  </StoreProvider>
);

ReactDOM.render(<Root />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
