import React, { useContext, useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import Strapi from 'strapi-sdk-javascript/build/main';

import { getToken } from '../utils';
import { Store } from '../store/store';
import useViewportWidth from '../hooks/useViewportWidth';

import CategoryList from './category/category-list/category-list';
import Checkout from './checkout/checkout';
import Footer from './shared/footer';
import Landing from './landing/landing';
import Navbar from './shared/navbar';
import NotFound from './shared/not-found';
import ProductDetail from './product/product-detail/product-detail';
import ProductList from './product/product-list/product-list';
import Signin from './signin/signin';
import Signup from './signup/signup';

import './app.sass';
import 'react-toastify/dist/ReactToastify.css';
import 'typicons.font/src/font/typicons.css';

const apiUrl = process.env.API_URL || 'http://localhost:1337';
const strapi = new Strapi(apiUrl);

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      getToken() !== null ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: '/signin',
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

const App = props => {
  const viewportWidth = useViewportWidth();
  const { state, dispatch } = useContext(Store); // eslint-disable-line
  const [loadingProducts, setLoadingProducts] = useState(true); // eslint-disable-line

  useEffect(
    () => {
      fetchCategories();
    },
    [] // eslint-disable-line
  );

  const fetchCategories = async () => {
    try {
      const response = await strapi.request('POST', '/graphql', {
        data: {
          query: `query {
              categories {
              id
              name
              products {
                id
                name
                description
                price
                category {
                  id
                  name
                }
                image {
                  url
                }
              }
            }
          }`
        }
      });

      const newCategories = response.data.categories.map(category => ({
        ...category,
        products: category.products.map(product => ({
          ...product,
          priceFormat: formattedPrice(product.price)
        }))
      }));

      dispatch({
        type: 'SET_CATEGORIES',
        payload: newCategories
      });
      setLoadingProducts(false);
    } catch (error) {
      console.error(error.message);
    }
  };

  const formattedPrice = price => {
    const [dollarAmount, centAmount] = price.toString().split('.');

    return {
      dollars: dollarAmount,
      cents: centAmount
    };
  };

  return (
    <Router>
      <>
        <Navbar />
        <Switch>
          <Route component={Landing} exact path="/" />
          <Route component={NotFound} path="/not-found" />
          <Route component={Signin} path="/signin" />
          <Route component={Signup} path="/signup" />
          <PrivateRoute component={Checkout} path="/checkout" />
          <Route component={CategoryList} exact path="/categories" />
          <Route component={ProductList} exact path="/categories/:categoryId" />
          <Route
            component={ProductDetail}
            exact
            path="/categories/:categoryId/products/:productId"
          />
          <Route component={NotFound} />
        </Switch>
        <Footer />
        <ToastContainer
          autoClose={3000}
          closeOnClick
          draggable
          hideProgressBar={false}
          newestOnTop
          pauseOnHover={false}
          pauseOnVisibilityChange
          position={viewportWidth < 768 ? 'bottom-center' : 'top-center'}
          rtl={false}
        />
      </>
    </Router>
  );
};

export default App;
