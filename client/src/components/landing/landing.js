import React, { useContext } from 'react';

import { Store } from '../../store/store';

import Hero from './hero/hero';
import BrowseCategories from './browse-categories/browse-categories';
import Testimonials from './testimonials/testimonials';
import About from './about/about';
import Contact from '../contact/contact';

import useViewportWidth from '../../hooks/useViewportWidth';

const Landing = () => {
  const viewportWidth = useViewportWidth();
  const { state } = useContext(Store);

  // TODO: Need top level enum to hold these values
  let categoriesPreviewCount;
  if (viewportWidth < 576) {
    categoriesPreviewCount = 3;
  } else if (viewportWidth < 768) {
    categoriesPreviewCount = 2;
  } else if (viewportWidth < 992) {
    categoriesPreviewCount = 3;
  } else if (viewportWidth < 1200) {
    categoriesPreviewCount = 4;
  } else {
    categoriesPreviewCount = 5;
  }

  const categoriesPreview = state.categories.map(category => {
    const productsPreview = [];
    for (let i = 0; i < categoriesPreviewCount; i++) {
      productsPreview.push(category.products[i]);
    }
    return { ...category, products: productsPreview };
  });

  return (
    <div className="landing">
      <Hero />
      <BrowseCategories categories={categoriesPreview} />
      <Testimonials />
      <About />
      <Contact />
    </div>
  );
};

export default Landing;
