import React from 'react';

const Hero = () => {
  const handleClick = event => {
    const categoryTeaser = document.querySelector('.browse-categories');
    categoryTeaser.scrollIntoView();
  };

  return (
    <div className="hero">
      <div className="hero__background-container">
        <div className="hero__background-overlay" />
        <img
          className="hero__background-image"
          src="https://res.cloudinary.com/mcb-1hunnit-studios/image/upload/v1558522143/island_x5djvy.jpg"
          alt="Deserted island"
        />
        <video
          autoPlay
          className="hero__background-video"
          loop
          muted
          poster="https://res.cloudinary.com/mcb-1hunnit-studios/image/upload/v1558522143/island_x5djvy.jpg">
          <source
            src="https://res.cloudinary.com/mcb-1hunnit-studios/video/upload/v1558522130/island_isjf2x.webm"
            type="video/webm"
            alt="Deserted island"
          />
        </video>
      </div>
      <div className="hero__content-wrapper">
        <div className="hero__copy-container">
          <h1 className="hero__title">
            <span>OC</span>
            &nbsp;
            <span>Aqua</span>
            &nbsp;
            <span>Supply</span>
          </h1>
          <p className="hero__subtitle">
            Your one-stop shop for all things aquatic
          </p>
        </div>
        <div className="hero__scroll-down-container">
          <span
            className="hero__scroll-down typcn typcn-arrow-down"
            onClick={handleClick}
          />
        </div>
      </div>
    </div>
  );
};

export default Hero;
