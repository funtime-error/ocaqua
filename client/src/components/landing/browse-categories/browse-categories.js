import React from 'react';
import { Link } from 'react-router-dom';

import Loader from '../../shared/loader';

import useViewportWidth from '../../../hooks/useViewportWidth';

const apiUrl = process.env.API_URL || 'http://localhost:1337';

const BrowseCategories = ({ categories }) => {
  const viewportWidth = useViewportWidth();

  return (
    <div className="browse-categories">
      <div className="content-wrapper">
        <h2 className="landing__title">Browse Categories</h2>
        {categories.map(category => (
          <div className="category-row" key={category.id}>
            <div className="category-row__category-header">
              <span className="category-row__category-name">
                {category.name} Products
              </span>
              {viewportWidth >= 576 ? (
                <span className="category-row__view-all">
                  <Link to={`/categories/${category.id}`}>
                    View all {category.name} products
                    <span className="typcn typcn-chevron-right" />
                  </Link>
                </span>
              ) : (
                <div className="category-row__view-all category-row__view-all--mobile">
                  <Link to={`/categories/${category.id}`}>
                    <span className="typcn typcn-chevron-right" />
                  </Link>
                </div>
              )}
            </div>
            <div className="category-row__product-list">
              {category.products.map(product => (
                <Link
                  className="category-row__product-card"
                  to={`/categories/${category.id}/products/${product.id}`}
                  key={product.id}>
                  <div className="category-row__product-container">
                    <div className="category-row__product-image-container">
                      <img
                        className="category-row__product-image"
                        src={`${apiUrl}${product.image[0].url}`}
                        alt={product.name}
                      />
                    </div>
                    <div className="category-row__product-detail">
                      <p className="category-row__product-name">
                        {product.name}
                      </p>
                      <p className="category-row__product-price">
                        <span>
                          <small>
                            <sup>$</sup>
                            {product.priceFormat.dollars}
                            <sup>{product.priceFormat.cents}</sup>
                          </small>
                        </span>
                      </p>
                    </div>
                  </div>
                </Link>
              ))}
            </div>
            {viewportWidth < 576 && (
              <span className="category-row__view-all">
                <Link to={`/categories/${category.id}`}>
                  View all {category.name} products&nbsp;&rsaquo;
                </Link>
              </span>
            )}
          </div>
        ))}
        <div className="category-row__category-footer">
          <span className="category-row__view-all">
            <Link to={`/categories`}>
              View all products
              <span className="typcn typcn-chevron-right" />
            </Link>
          </span>
        </div>
      </div>
      {!categories.length && (
        <div className="categories__loading">
          <p className="categories__loading-copy">Loading categories</p>
          <Loader show={!categories.length} />
        </div>
      )}
    </div>
  );
};

export default BrowseCategories;
