import React from 'react';

import billGates from './bill-gates.jpg';

const Testimonials = () => (
  // TODO: Clean up unnecessary nesting
  <div className="testimonials">
    <div className="content-wrapper">
      <div className="testimonials__card">
        <div className="testimonials__card-content">
          <div className="testimonials__card__quote-container">
            <span className="testimonials__card__quotation-mark">&ldquo;</span>
            <blockquote className="testimonials__card__quote-block">
              <span className="testimonials__card__quote-text">
                If you cannot do it well, at least make it look good.{' '}
                <span>OC Aqua</span> does both, and does both well.
              </span>
            </blockquote>
          </div>
          <div className="testimonials__card__reviewer-container">
            <img
              className="testimonials__card__reviewer-image"
              src={billGates}
              alt="Bill Gates"
            />
            <div className="testimonials__card__reviewer-details">
              <p className="testimonials__card__reviewer-name">Bill Gates</p>
              <p className="testimonials__card__reviewer-title">
                Founder and Technology Advisor, Microsoft
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Testimonials;
