import React, { useState } from 'react';
import { createBrowserHistory } from 'history';
import Autosuggest from 'react-autosuggest';

const SearchBar = ({ categories }) => {
  const history = createBrowserHistory({
    forceRefresh: true
  });
  const [searchTerm, setSearchTerm] = useState('');
  const [suggestions, setSuggestions] = useState([]);

  const allProducts = categories
    .map(category => category.products)
    .reduce((accum, val) => accum.concat(val), []);

  const getSuggestions = value => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return inputLength === 0
      ? []
      : allProducts.filter(product =>
          product.name.toLowerCase().includes(inputValue)
        );
  };

  const getSuggestionValue = suggestion => suggestion.name;

  const handleSuggestionClick = suggestion => {
    history.push(
      `/categories/${suggestion.category.id}/products/${suggestion.id}`
    );
  };

  const renderSuggestion = suggestion => (
    <div onClick={() => handleSuggestionClick(suggestion)}>
      {suggestion.name}
    </div>
  );

  const onChange = (event, { newValue }) => {
    setSearchTerm(newValue);
  };

  const onSuggestionsFetchRequested = ({ value }) => {
    setSuggestions(getSuggestions(value));
  };

  const onSuggestionsClearRequested = () => {
    setSuggestions([]);
  };

  const inputProps = {
    placeholder: 'Search the store',
    value: searchTerm,
    onChange
  };

  return (
    <Autosuggest
      suggestions={suggestions}
      onSuggestionsFetchRequested={onSuggestionsFetchRequested}
      onSuggestionsClearRequested={onSuggestionsClearRequested}
      getSuggestionValue={getSuggestionValue}
      renderSuggestion={renderSuggestion}
      inputProps={inputProps}
    />
  );
};

export default SearchBar;
