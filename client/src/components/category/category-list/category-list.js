import React from 'react';
import debounce from 'lodash-es/debounce';
import Strapi from 'strapi-sdk-javascript/build/main';
import { Link } from 'react-router-dom';

const apiUrl = process.env.API_URL || 'http://localhost:1337';
const strapi = new Strapi(apiUrl);

class CategoryList extends React.Component {
  state = {
    products: [],
    searchTerm: ''
  };

  async componentDidMount() {
    try {
      const response = await strapi.request('POST', '/graphql', {
        data: {
          query: `query {
            categories {
              id
              name
              products {
                id
                name
                price
                category{
                  id
                  name
                }
                image {
                  url
                }
              }
            }
          }
          `
        }
      });

      const allProducts = response.data.categories
        .map(category => category.products)
        .reduce((accum, val) => accum.concat(val), []);

      this.setState({
        products: allProducts
      });
    } catch (error) {
      console.error(error);
    }
  }

  handleSearchTermChange = ({ target: { value } }) => {
    this.setState({ searchTerm: value }, () => {
      debounce(this.searchProducts, 300)();
    });
  };

  searchProducts = async () => {
    const response = await strapi.request('POST', '/graphql', {
      data: {
        query: `query {
          products(where: {
            name_contains: "${this.state.searchTerm}"
          }) {
            id
            name
            price
            category{
              id
              name
            }
            image {
              url
            }
          }
        }`
      }
    });

    this.setState({
      products: response.data.products
    });
  };

  render() {
    const { products } = this.state;

    const productsDisplay = products.map(product => {
      return (
        <Link
          className="product-list__product-card"
          to={`/categories/${product.category.id}/products/${product.id}`}
          key={product.id}>
          <div className="product-list__product-container">
            <div className="product-list__product-image-container">
              <img
                className="product-list__product-image"
                src={`${apiUrl}${product.image[0].url}`}
                alt={product.name}
              />
            </div>
            <div className="product-list__product-detail">
              <p className="product-list__product-name">{product.name}</p>
              <p className="product-list__product-price">
                <FormattedPrice price={product.price} />
              </p>
            </div>
          </div>
        </Link>
      );
    });

    return (
      <div className="category-list product-list">
        <div className="content-wrapper">
          <div className="category-list__header">
            <h2 className="category-list__title">All Products</h2>
            <input
              className="product-list__search-bar"
              type="search"
              value={this.state.searchTerm}
              onChange={this.handleSearchTermChange}
              placeholder="Search all products"
            />
          </div>
          {productsDisplay.length ? (
            <div className="product-list__product-grid">{productsDisplay}</div>
          ) : (
            <p>No products found.</p>
          )}
        </div>
      </div>
    );
  }
}

const FormattedPrice = ({ price }) => {
  const [dollarAmount, centAmount] = price
    .toString()
    .split('.')
    .map(item => +item);

  return (
    <span>
      <small>
        <sup>$</sup>
        {dollarAmount}
        <sup>{centAmount}</sup>
      </small>
    </span>
  );
};

export default CategoryList;
