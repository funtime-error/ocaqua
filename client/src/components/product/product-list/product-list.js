import React from 'react';
import Strapi from 'strapi-sdk-javascript/build/main';
import { Link } from 'react-router-dom';

import { getCart } from '../../../utils';

const apiUrl = process.env.API_URL || 'http://localhost:1337';
const strapi = new Strapi(apiUrl);

class ProductList extends React.Component {
  state = {
    products: [],
    category: {},
    cartItems: [],
    searchTerm: ''
  };

  async componentDidMount() {
    try {
      const response = await strapi.request('POST', '/graphql', {
        data: {
          query: `query {
            category(id: "${this.props.match.params.categoryId}") {
              id
              name
              description
              image {
                url
              }
              products {
                id
                name
                description
                image {
                  url
                }
                price
              }
            }
          }`
        }
      });

      this.setState({
        products: response.data.category.products,
        category: response.data.category,
        cartItems: getCart()
      });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { category, products } = this.state;

    const productsDisplay = products.map(product => {
      return (
        <Link
          className="product-list__product-card"
          to={`/categories/${category.id}/products/${product.id}`}
          key={product.id}>
          <div className="product-list__product-container">
            <div className="product-list__product-image-container">
              <img
                className="product-list__product-image"
                src={`${apiUrl}${product.image[0].url}`}
                alt={product.name}
              />
            </div>
            <div className="product-list__product-detail">
              <p className="product-list__product-name">{product.name}</p>
              <p className="product-list__product-price">
                <FormattedPrice price={product.price} />
              </p>
            </div>
          </div>
        </Link>
      );
    });

    return (
      <div className="product-list">
        <div className="content-wrapper">
          <h2 className="product-list__category-name">
            {category.name} Products
          </h2>
          <div className="product-list__product-grid">{productsDisplay}</div>
        </div>
      </div>
    );
  }
}

const FormattedPrice = ({ price }) => {
  const [dollarAmount, centAmount] = price
    .toString()
    .split('.')
    .map(item => +item);

  return (
    <span>
      <small>
        <sup>$</sup>
        {dollarAmount}
        <sup>{centAmount}</sup>
      </small>
    </span>
  );
};

export default ProductList;
