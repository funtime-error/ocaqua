import React, { useContext, useEffect, useState } from 'react';
import Strapi from 'strapi-sdk-javascript/build/main';
import { Carousel } from 'react-responsive-carousel';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

import { getToken, getCart, setCart } from '../../../utils';
import { Store } from '../../../store/store';

import 'react-responsive-carousel/lib/styles/carousel.min.css';

const apiUrl = process.env.API_URL || 'http://localhost:1337';
const strapi = new Strapi(apiUrl);

const ProductDetailHeading = ({ history, product }) => {
  const store = useContext(Store);
  const { state, dispatch } = store; // eslint-disable-line
  const [quantity, setQuantity] = useState(1);
  const [cartItems, setCartItems] = useState([]);

  useEffect(() => {
    setCartItems(getCart());
  }, []);

  const incrementQuantity = event => {
    event.preventDefault();
    setQuantity(quantity + 1);
  };

  const decrementQuantity = event => {
    event.preventDefault();
    if (quantity > 0) setQuantity(quantity - 1);
  };

  const handleChangeQuantity = event => {
    event.persist();
    setQuantity(parseInt(event.target.value));
  };

  const addToCart = (product, quantity) => {
    if (quantity <= 0) return;

    let updatedItems;
    const alreadyInCart = cartItems.findIndex(item => item.id === product.id);

    if (alreadyInCart === -1) {
      updatedItems = cartItems.concat({
        ...product,
        quantity
      });
      setCartItems(updatedItems);
      setCart(updatedItems);
    } else {
      updatedItems = [...cartItems];
      updatedItems[alreadyInCart].quantity += quantity;
      setCartItems(updatedItems);
      setCart(updatedItems);
    }

    dispatch({
      type: 'SET_CART',
      payload: updatedItems
    });

    toast.info(`Added ${quantity} x ${product.name} to your cart.`, {
      onClose: history.push('/categories')
    });
  };

  const handleSubmitToCart = event => {
    event.preventDefault();
  };

  return (
    <div className="product-detail-heading">
      <div className="product-detail-heading__title">
        <span>{product.name}</span>
        <span>${product.price}</span>
      </div>
      {getToken() !== null ? (
        <form
          onSubmit={handleSubmitToCart}
          className="product-detail-heading__cart-update">
          <fieldset className="product-detail-heading__inputs">
            <button
              className="product-detail-heading__counter-button"
              onClick={decrementQuantity}
              disabled={quantity <= 0}>
              -
            </button>
            <input
              type="number"
              className="product-detail-heading__counter"
              id="number"
              value={quantity}
              onChange={handleChangeQuantity}
            />
            <button
              className="product-detail-heading__counter-button"
              onClick={incrementQuantity}>
              +
            </button>
          </fieldset>
          <fieldset className="product-detail-heading__submit">
            <button
              className="product-detail-heading__add-to-cart"
              type="submit"
              onClick={() => addToCart(product, quantity)}
              disabled={!getToken() || quantity <= 0 || isNaN(quantity)}>
              <span className="typcn typcn-shopping-cart" /> Add to Cart
            </button>
          </fieldset>
        </form>
      ) : (
        <Link to="/signin">
          <p className="product-detail-heading__login">Log in to add to cart</p>
        </Link>
      )}
    </div>
  );
};

const ProductDetail = props => {
  const [product, setProduct] = useState({});
  const [productImages, setProductImages] = useState([]);

  const fetchProduct = async () => {
    try {
      const response = await strapi.request('POST', '/graphql', {
        data: {
          query: `query {
            product(id: "${props.match.params.productId}") {
              id
              name
              description
              specifications
              image {
                url
              }
              price
            }
          }`
        }
      });

      setProduct(response.data.product);
      setProductImages(response.data.product.image);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchProduct();
  }, []); // eslint-disable-line

  return (
    <div className="product-detail">
      <div className="content-wrapper">
        <h2 className="product-detail__title">{product.name}</h2>
        <Carousel autoPlay infiniteLoop className="product-detail-carousel">
          {productImages.length &&
            productImages.map(image => (
              <div key={image.url}>
                <img
                  src={product.image && `${apiUrl}${image.url}`}
                  alt="Carousel product display"
                />
              </div>
            ))}
        </Carousel>
        <ProductDetailHeading history={props.history} product={product} />
        <div className="product-detail-info">
          <div className="product-detail-info__section">
            <h3 className="product-detail-info__section__title">Description</h3>
            <p className="product-detail-info__section__copy product-detail-info__section__copy--centered">
              {product.description}
            </p>
          </div>
          <div className="product-detail-info__section">
            <h3 className="product-detail-info__section__title">
              Specifications
            </h3>
            <p className="product-detail-info__section__copy">
              {product.specifications}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(ProductDetail);
