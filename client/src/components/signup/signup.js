import React from 'react';
import { Link } from 'react-router-dom';
import Strapi from 'strapi-sdk-javascript/build/main';
import { toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';

import Loader from '../shared/loader';
import { setToken } from '../../utils';

const apiUrl = process.env.API_URL || 'http://localhost:1337';
const strapi = new Strapi(apiUrl);

class Signup extends React.Component {
  state = {
    username: '',
    email: '',
    password: '',
    toast: false,
    toastMessage: '',
    loading: false
  };

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSubmit = async event => {
    const { username, email, password } = this.state;

    event.preventDefault();

    // TODO: Need better form management and validation
    if (this.isFormEmpty(this.state)) {
      toast.error('Please in all fields');
      return;
    }

    try {
      this.setState({ loading: true });
      const response = await strapi.register(username, email, password);
      setToken(response.jwt);
      toast.success(`Welcome, ${username}!`, {
        autoClose: 2400,
        onClose: () => {
          this.setState({ loading: false }, () => this.redirectUser('/'));
        }
      });
    } catch (error) {
      this.setState({ loading: false });
      toast.error(error.message);
    }
  };

  redirectUser = path => {
    this.props.history.push(path);
  };

  isFormEmpty = ({ username, email, password }) => {
    return !username || !email || !password;
  };

  render() {
    const { loading, username, email, password } = this.state;

    return (
      <div className="signup">
        <div className="content-wrapper">
          <form className="signup__form" onSubmit={this.handleSubmit}>
            <h2 className="signup__form-title">Create Account</h2>
            <input
              type="text"
              name="username"
              id="username"
              placeholder="Username"
              onChange={this.handleChange}
              value={username}
            />
            <input
              type="email"
              name="email"
              id="email"
              placeholder="Email"
              onChange={this.handleChange}
              value={email}
            />
            <input
              type="password"
              name="password"
              id="password"
              placeholder="Password"
              onChange={this.handleChange}
              value={password}
            />
            <button className="signup__form-submit" type="submit">
              Sign Up <Loader show={loading} inline />
            </button>
            <p className="signup__return-to-store">
              or <Link to="/">Return to Store</Link>
            </p>
          </form>
        </div>
      </div>
    );
  }
}

export default Signup;
