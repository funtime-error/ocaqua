import React, { useContext, useState } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';

import { Store } from '../../store/store';

import BurgerMenu from './burger-menu';
import SearchBar from '../search-bar/search-bar';

import {
  getUsername,
  getToken,
  getCart,
  clearToken,
  clearCart
} from '../../utils';

const Navbar = props => {
  const store = useContext(Store);
  const { state, dispatch } = store;
  const [loading, setLoading] = useState(false);

  const handleSignout = () => {
    setLoading(true);
    setTimeout(() => {
      clearToken();
      clearCart();
      dispatch({
        type: 'SET_CART',
        payload: getCart()
      });
      setLoading(false);
      props.history.push('/');
      toast('See you again soon 👋', {
        className: 'toaster'
      });
    }, 1000);
  };
  const cartItemCount = () => {
    const quantities = state.cart.map(item => item.quantity);
    return quantities.reduce((accum, nextVal) => accum + nextVal, 0);
  };
  return (
    <nav className="navbar">
      <NavLink className="navbar__logo" activeClassName="active" exact to="/">
        <img
          className="navbar__logo-icon"
          src="/icons/shrimp-logo.png"
          alt="Shrimp logo"
        />
        <div className="navbar__logo-text">
          OC<span>Aqua</span>
        </div>
      </NavLink>
      <div className="navbar__interaction">
        <SearchBar categories={state.categories} />
        <BurgerMenu
          cartItemCount={cartItemCount()}
          cartItems={state.cart}
          handleSignout={handleSignout}
          isLoggedIn={getToken()}
          loading={loading}
          user={getUsername()}
        />
      </div>
    </nav>
  );
};
export default withRouter(Navbar);
