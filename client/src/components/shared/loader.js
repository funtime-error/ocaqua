import React from 'react';
import { PulseLoader } from 'react-spinners';

const Loader = ({ show, inline = false }) =>
  show && (
    <div className={`loader ${inline && 'loader--inline'}`}>
      <PulseLoader color="orchid" css={{ margin: '0 auto' }} size={4} />
    </div>
  );

export default Loader;
