import React, { useState } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { slide as Menu } from 'react-burger-menu';

import Loader from './loader';

import useViewportWidth from '../../hooks/useViewportWidth';
import {
  CART_ENUM,
  calculateCartItemsSubtotalPrice,
  calculateCartItemsTotalPrice
} from '../../utils';

const BurgerMenu = props => {
  const [isOpen, setIsOpen] = useState(false);

  const viewportWidth = useViewportWidth();

  const subtotal = parseFloat(calculateCartItemsSubtotalPrice(props.cartItems));
  const calculatedTax = (subtotal + CART_ENUM.SHIPPING) * CART_ENUM.SALES_TAX;

  const handleNavItemClick = () => {
    setIsOpen(false);
  };

  const isMenuOpen = menuState => {
    if (menuState.isOpen === isOpen) return;
    setIsOpen(menuState.isOpen);
  };

  return (
    <Menu
      className="burger-menu"
      isOpen={isOpen}
      onStateChange={state => isMenuOpen(state)}
      right
      width={viewportWidth < 768 ? '100%' : '480px'}
      {...props}>
      {props.isLoggedIn && props.user ? (
        <div className="burger-menu__heading">
          <h3 className="burger-menu__auth-status">
            Logged in as <i>{props.user}</i>
          </h3>
          <span className="burger-menu__heading-copy">
            <button
              className="burger-menu__signout burger-menu__auth-action"
              onClick={() => {
                props.handleSignout();
                setTimeout(() => {
                  handleNavItemClick();
                }, 1000);
              }}>
              Log out <Loader show={props.loading} inline />
            </button>
          </span>
        </div>
      ) : (
        <div className="burger-menu__heading">
          <span className="burger-menu__heading-copy">
            <NavLink
              className="burger-menu__auth-action"
              activeClassName="active"
              to="/signin"
              onClick={handleNavItemClick}>
              Sign in
            </NavLink>
          </span>
          <span className="burger-menu__heading-copy">&nbsp;or&nbsp;</span>
          <span className="burger-menu__heading-copy">
            <NavLink
              className="burger-menu__auth-action"
              activeClassName="active"
              to="/signup"
              onClick={handleNavItemClick}>
              Create an Account
            </NavLink>
          </span>
        </div>
      )}
      <div className="burger-menu__nav-items">
        <h3 className="burger-menu__nav-title">Navigation</h3>
        <div className="burger-menu__nav-item-row">
          <NavLink
            activeClassName="active"
            className="burger-menu__nav-item"
            to="/"
            onClick={handleNavItemClick}>
            &rsaquo;&nbsp;Home
          </NavLink>
        </div>
        <div className="burger-menu__nav-item-row">
          <NavLink
            activeClassName="active"
            className="burger-menu__nav-item"
            to="/categories"
            onClick={handleNavItemClick}>
            &rsaquo;&nbsp;Shop
          </NavLink>
        </div>
        <div className="burger-menu__nav-item-row">
          <NavLink
            activeClassName="active"
            className="burger-menu__nav-item"
            to="/checkout"
            onClick={handleNavItemClick}>
            &rsaquo;&nbsp;Checkout
            {!!props.cartItemCount && (
              <span>&nbsp;({props.cartItemCount})</span>
            )}
          </NavLink>
        </div>
      </div>
      {props.isLoggedIn && props.user && props.cartItemCount && (
        <div className="burger-menu__cart-content">
          <h3 className="burger-menu__cart-title">Your Cart</h3>
          <div className="burger-menu__cart-items-wrapper">
            <table className="burger-menu__cart-items">
              <tbody className="burger-menu__cart-product-items">
                {props.cartItems.map((item, index) => (
                  <tr key={item.id} className="burger-menu__cart-item">
                    <td align="left" colSpan="2">
                      {item.name}
                    </td>
                    <td
                      align="left"
                      className="burger-menu__cart-row-quantity"
                      colSpan="1">
                      &#215;&nbsp;{item.quantity}
                    </td>
                    <td
                      align="right"
                      className="burger-menu__cart-row-subtotal"
                      colSpan="1">
                      ${(item.quantity * item.price).toFixed(2)}
                    </td>
                  </tr>
                ))}
              </tbody>
              <tbody className="burger-menu__cart-subtotal-calculations">
                <tr className="burger-menu__cart-subtotal">
                  <td align="left" colSpan="2">
                    Subtotal
                  </td>
                  <td align="right" colSpan="2">
                    ${subtotal.toFixed(2)}
                  </td>
                </tr>
                <tr className="burger-menu__cart-shipping">
                  <td align="left" colSpan="2">
                    Shipping
                  </td>
                  <td align="right" colSpan="2">
                    ${CART_ENUM.SHIPPING}
                  </td>
                </tr>
                <tr className="burger-menu__cart-tax">
                  <td align="left" colSpan="2">
                    Tax
                  </td>
                  <td align="right" colSpan="2">
                    ${calculatedTax.toFixed(2)}
                  </td>
                </tr>
              </tbody>
              <tbody className="burger-menu__cart-total-row">
                <tr className="burger-menu__cart-total">
                  <td align="left" colSpan="2">
                    Total
                  </td>
                  <td align="right" colSpan="2">
                    $
                    {calculateCartItemsTotalPrice(
                      subtotal,
                      CART_ENUM.SHIPPING,
                      calculatedTax
                    ).toFixed(2)}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="burger-menu__proceed-to-checkout">
            <NavLink to="/checkout" onClick={handleNavItemClick}>
              Proceed to checkout&nbsp;&rsaquo;
            </NavLink>
          </div>
        </div>
      )}
    </Menu>
  );
};

export default withRouter(BurgerMenu);
