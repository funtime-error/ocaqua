import React from 'react';

import constructionSign from './images/construction-sign.png';

const NotFound = () => (
  <div className="not-found">
    <div className="content-wrapper">
      <h2 className="not-found__title">Oops.</h2>
      <div className="not-found__image-wrapper">
        <img
          className="not-found__image"
          src={constructionSign}
          alt="Under Construction"
        />
      </div>
      <div className="not-found__copy-container">
        <h3 className="not-found__copy-heading">We're working on it!</h3>
        <p className="not-found__copy">
          Thank you for being patient. We are currently doing some work on the
          site and will be back shortly.
        </p>
      </div>
    </div>
  </div>
);

export default NotFound;
