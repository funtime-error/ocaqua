import React from 'react';

const Footer = () => (
  <footer className="footer">
    <div className="content-wrapper">
      <p className="footer__copy">
        &copy; 2019 MCB | Level Up Labs | 1hunnit Studios | Senpire | Silk
        Alchemy | OC Aqua Supply
      </p>
    </div>
  </footer>
);

export default Footer;
