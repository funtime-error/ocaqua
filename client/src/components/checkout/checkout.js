import React, { useContext } from 'react';
import { Elements, StripeProvider, injectStripe } from 'react-stripe-elements';
import { withRouter } from 'react-router-dom';

import CheckoutCart from './checkout-cart/checkout-cart';
import CheckoutOrder from './checkout-order/checkout-order';

import { Store } from '../../store/store';
import useViewportWidth from '../../hooks/useViewportWidth';

const _CheckoutForm = props => {
  const store = useContext(Store);
  const { state, dispatch } = store;

  const viewportWidth = useViewportWidth();

  return (
    <div className="checkout">
      <div className="checkout__content-container content-wrapper">
        <CheckoutCart
          cartItems={state.cart}
          dispatch={dispatch}
          viewportWidth={viewportWidth}
        />
        <CheckoutOrder
          cartItems={state.cart}
          dispatch={dispatch}
          history={props.history}
          stripe={props.stripe}
        />
      </div>
    </div>
  );
};

const CheckoutForm = withRouter(injectStripe(_CheckoutForm));

class Checkout extends React.Component {
  state = {
    stripe: null
  };

  componentDidMount() {
    // TODO: Extract api key
    if (window.Stripe) {
      this.setState({
        stripe: window.Stripe('pk_test_0PpFlTlYHrab5aU68KWl8xhm00cgW5qnWe')
      });
    } else {
      document.querySelector('#stripe-js').addEventListener('load', () => {
        this.setState({
          stripe: window.Stripe('pk_test_0PpFlTlYHrab5aU68KWl8xhm00cgW5qnWe')
        });
      });
    }
  }

  render() {
    return (
      <StripeProvider stripe={this.state.stripe}>
        <Elements
          fonts={[
            {
              cssSrc:
                'https://fonts.googleapis.com/css?family=Montserrat:300,400'
            }
          ]}>
          <CheckoutForm />
        </Elements>
      </StripeProvider>
    );
  }
}

export default Checkout;
