import React, { createRef, useRef, useState } from 'react';
import { Link } from 'react-router-dom';

import Loader from '../../shared/loader';

import {
  CART_ENUM,
  calculateCartItemsSubtotalPrice,
  calculateCartItemsTotalPrice,
  setCart
} from '../../../utils';

const CheckoutCart = ({ cartItems, dispatch, viewportWidth }) => {
  const subtotal = parseFloat(calculateCartItemsSubtotalPrice(cartItems));
  const calculatedTax = (subtotal + CART_ENUM.SHIPPING) * CART_ENUM.SALES_TAX;
  const [editing, setEditing] = useState(false);
  const [currentCart, setCurrentCart] = useState(cartItems);

  const quantityRefs = useRef(cartItems.map(cartItem => createRef()));

  const continueEdit = rowIndex => {
    setEditing(true);
    quantityRefs.current[rowIndex].current.focus();
  };

  const handleBlur = event => {
    const itemsWithQuantity = currentCart.filter(item => item.quantity > 0);
    setEditing(false);
    setCurrentCart(itemsWithQuantity);
    dispatch({
      type: 'SET_CART',
      payload: itemsWithQuantity
    });
    setCart(itemsWithQuantity);
  };

  const handleQuantityChange = event => {
    event.persist();

    const cartItem = JSON.parse(event.target.dataset.cartItem);
    const currentCartItems = Array.from(cartItems);
    const cartItemIndex = currentCartItems.findIndex(
      item => item.id === cartItem.id
    );

    currentCartItems[cartItemIndex].quantity = !event.target.value
      ? 0
      : parseInt(event.target.value);
    quantityRefs.current[cartItemIndex].current.value =
      currentCartItems[cartItemIndex].quantity;

    setCurrentCart(currentCartItems);
  };

  const deleteItemFromCart = itemToDeleteId => {
    const filteredItems = cartItems.filter(item => item.id !== itemToDeleteId);
    setCurrentCart(filteredItems);
    dispatch({
      type: 'SET_CART',
      payload: filteredItems
    });
    setCart(filteredItems);
  };

  // const closeModal = () => this.setState({ modal: false });

  return (
    <div className="checkout-cart">
      <div className="checkout-cart__cart-content">
        <h2 className="checkout-cart__title">Cart</h2>
        <div className="checkout-cart__items-wrapper">
          {cartItems.length ? (
            <>
              <table className="checkout-cart__items">
                <tbody>
                  {currentCart.map((item, index) => (
                    <React.Fragment key={item.id}>
                      <tr className="checkout-cart__item checkout-cart__item__name">
                        <td align="left" colSpan="6">
                          {item.name}
                        </td>
                      </tr>
                      <tr className="checkout-cart__item checkout-cart__item__info">
                        <td colSpan="2">
                          &#215;
                          <input
                            className="checkout-cart__quantity"
                            data-cart-item={JSON.stringify(item)}
                            id="quantity"
                            min="0"
                            name="quantity"
                            onBlur={handleBlur}
                            onChange={handleQuantityChange}
                            onFocus={() => continueEdit(index)}
                            readOnly={!editing}
                            ref={quantityRefs.current[index]}
                            type="number"
                            value={parseInt(item.quantity)}
                          />
                        </td>
                        <td colSpan="1" align="center">
                          {!editing ? (
                            <span
                              className="typcn typcn-pencil checkout-cart__edit-quantity"
                              onClick={() => continueEdit(index)}
                              title={`Update ${item.name} quantity`}
                            />
                          ) : (
                            <Loader
                              className="checkout-cart__editing-loader"
                              show={editing}
                              inline
                            />
                          )}
                        </td>
                        <td colSpan="2" className="checkout-cart__row-subtotal">
                          ${(item.quantity * item.price).toFixed(2)}
                        </td>
                        <td colSpan="1" align="center">
                          <span
                            className="typcn typcn-delete-outline checkout-cart__delete-item"
                            onClick={() => deleteItemFromCart(item.id)}
                            title={`Remove ${item.name} from cart entirely`}
                          />
                        </td>
                      </tr>
                    </React.Fragment>
                  ))}
                  <tr className="checkout-cart__subtotal">
                    <td align="left" colSpan="3">
                      Subtotal
                    </td>
                    <td colSpan="2">${subtotal.toFixed(2)}</td>
                  </tr>
                  <tr className="checkout-cart__shipping">
                    <td align="left" colSpan="3">
                      Shipping
                    </td>
                    <td colSpan="2">${CART_ENUM.SHIPPING}</td>
                  </tr>
                  <tr className="checkout-cart__tax">
                    <td align="left" colSpan="3">
                      Tax
                    </td>
                    <td colSpan="2">${calculatedTax.toFixed(2)}</td>
                  </tr>
                  <tr className="checkout-cart__total">
                    <td align="left" colSpan="3">
                      Total
                    </td>
                    <td colSpan="2">
                      $
                      {calculateCartItemsTotalPrice(
                        subtotal,
                        CART_ENUM.SHIPPING,
                        calculatedTax
                      ).toFixed(2)}
                    </td>
                  </tr>
                </tbody>
              </table>
            </>
          ) : (
            <>
              <p className="checkout-cart__no-items">
                You have no items in your cart.
              </p>
              <p className="checkout-cart__no-items">
                <Link to="/">
                  <span>Return to store?</span>
                </Link>
              </p>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default CheckoutCart;
