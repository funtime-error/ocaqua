import React, { useState } from 'react';
import { toast } from 'react-toastify';
import {
  CardNumberElement,
  CardExpiryElement,
  CardCVCElement
} from 'react-stripe-elements';
import Strapi from 'strapi-sdk-javascript/build/main';

import Loader from '../../shared/loader';

import {
  calculateCartItemsSubtotalPrice,
  calculateCartItemsTotalPrice,
  clearCart,
  getToken
} from '../../../utils';

import 'react-datepicker/dist/react-datepicker.css';

const apiUrl = process.env.API_URL || 'http://localhost:1337';
const strapi = new Strapi(apiUrl);

const CheckoutOrder = ({ cartItems, dispatch, history, stripe }) => {
  const [orderForm, setOrderForm] = useState({
    address: '',
    city: '',
    confirmationEmail: '',
    customerName: '',
    postalCode: '',
    phoneNumber: ''
  });
  const [orderProcessing, setOrderProcessing] = useState(false);

  const handleChange = event => {
    event.persist();
    if (Object.keys(orderForm).includes(event.target.name)) {
      setOrderForm({ ...orderForm, [event.target.name]: event.target.value });
    }
  };

  const handleSubmitOrder = async event => {
    event.preventDefault();

    const {
      customerName,
      address,
      postalCode,
      city,
      confirmationEmail,
      phoneNumber
    } = orderForm;
    const subtotal = calculateCartItemsSubtotalPrice(cartItems);
    const amount = calculateCartItemsTotalPrice(subtotal);
    let token;

    setOrderProcessing(true);

    try {
      const currentDateTime = new Date(Date.now());
      const response = await stripe.createToken();
      token = response.token.id;

      await strapi.createEntry('orders', {
        amount,
        products: cartItems,
        city,
        postalCode,
        address,
        customerName,
        phoneNumber,
        confirmationEmail,
        token
      });

      const orderMessage = `
      A delivery order has been placed.

      Contact:
      Name: ${customerName}
      Email: ${confirmationEmail}
      Phone number: ${phoneNumber}

      Order:
      ${JSON.stringify(cartItems, null, 2)}

      Delivery:
      Address: ${address}
      City: ${city}
      ZIP code: ${postalCode}
      `;

      const customerOrder = cartItems.map(cartItem => ({
        name: cartItem.name,
        price: cartItem.price,
        quantity: cartItem.quantity
      }));

      await strapi.request('POST', '/email', {
        data: {
          to: 'tester.senpai100@gmail.com',
          subject: `Order placed by ${customerName}: ${currentDateTime}`,
          text: orderMessage,
          html: `
          <h2>An order has been placed.</h2>
          <br>
          <h3>Contact:</h3>
          <p>Name: ${customerName}</p>
          <p>Email: ${confirmationEmail}</p>
          <p>Phone number: ${phoneNumber}</p>
          <div style="display: block">
            <h3>Delivery:</h3>
            <p>Address: ${address}</p>
            <p>City: ${city}</p>
            <p>ZIP code: ${postalCode}</p>
          </div>
          <br>
          <h3>Order</h3>
          <pre>${JSON.stringify(customerOrder, null, 2)}</pre>
          `
        }
      });

      await strapi.request('POST', '/email', {
        data: {
          to: confirmationEmail,
          subject: `Order Confirmation: OCAqua ${currentDateTime}`,
          text: 'Your order has been processed.',
          html: `
          <h2>Your order has been placed.</h2>
          <p style="display: block">Your order has been received and will be shipped as soon as it has been processed. Thank you for your patience!</p>
          <br>
          <h3>Order</h3>
          <pre>${JSON.stringify(customerOrder, null, 2)}</pre>
          `
        }
      });
      setOrderProcessing(false);
      clearCart();
      dispatch({
        type: 'SET_CART',
        payload: []
      });
      toast.success('Your order has been placed. Thank you!', {
        onClose: history.push('/')
      });
    } catch (error) {
      setOrderProcessing(false);
      console.error(JSON.stringify(error.message, null, 2));
      toast.error('Please fill out the form with valid information');
    }
  };

  const isFormIncomplete = ({
    customerName,
    address,
    postalCode,
    city,
    confirmationEmail,
    phoneNumber,
    pickupDateTime
  }) => {
    return (
      !customerName ||
      !confirmationEmail ||
      !phoneNumber ||
      !address ||
      !city ||
      !postalCode
    );
  };

  return (
    <div className="checkout-order">
      {getToken() !== null ? (
        <form onSubmit={handleSubmitOrder} className="checkout-order__form">
          <h2 className="checkout-order__form-title">Order Information</h2>
          <fieldset
            className="checkout-order__field-set"
            disabled={!cartItems.length}>
            <p className="checkout-order__form__stripe-label">
              Contact Information
            </p>
            <input
              type="text"
              name="customerName"
              id="customerName"
              placeholder="Name"
              onChange={handleChange}
              value={orderForm.customerName}
            />
            <input
              type="email"
              name="confirmationEmail"
              id="confirmationEmail"
              placeholder="Email"
              onChange={handleChange}
              value={orderForm.confirmationEmail}
            />
            <input
              type="tel"
              name="phoneNumber"
              id="phoneNumber"
              placeholder="Phone number"
              onChange={handleChange}
              value={orderForm.phoneNumber}
            />
            <p className="checkout-order__form__stripe-label">
              Shipping Information
            </p>
            <input
              type="text"
              name="address"
              id="address"
              placeholder="Address"
              onChange={handleChange}
              value={orderForm.address}
            />
            <input
              type="text"
              name="city"
              id="city"
              placeholder="City"
              onChange={handleChange}
              value={orderForm.city}
            />
            <input
              type="text"
              name="postalCode"
              id="text"
              placeholder="ZIP code"
              onChange={handleChange}
              value={orderForm.postalCode}
            />
            <p className="checkout-order__form__stripe-label">
              Payment Information
            </p>
            <div className="checkout-order__payment-form-group">
              <CardNumberElement
                id="stripe__input"
                disabled={!cartItems.length}
                style={{
                  base: {
                    fontFamily: '"Montserrat", sans-serif',
                    fontWeight: 300
                  }
                }}
              />
              <div className="checkout-order__payment-form-group-inline">
                <CardExpiryElement
                  id="stripe__input"
                  disabled={!cartItems.length}
                  style={{
                    base: {
                      fontFamily: '"Montserrat", sans-serif',
                      fontWeight: 300
                    }
                  }}
                />
                <CardCVCElement
                  id="stripe__input"
                  disabled={!cartItems.length}
                  style={{
                    base: {
                      fontFamily: '"Montserrat", sans-serif',
                      fontWeight: 300
                    }
                  }}
                />
              </div>
            </div>
            <button
              className="checkout-order__form-submit"
              disabled={
                orderProcessing || !getToken() || isFormIncomplete(orderForm)
              }
              onClick={handleSubmitOrder}>
              Place Order <Loader show={orderProcessing} inline />
            </button>
          </fieldset>
        </form>
      ) : (
        <p>Nothing to see here.</p>
      )}
    </div>
  );
};

export default CheckoutOrder;
