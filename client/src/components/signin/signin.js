import React from 'react';
import { Link } from 'react-router-dom';
import Strapi from 'strapi-sdk-javascript/build/main';
import { toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';

import Loader from '../shared/loader';
import { setToken, setUsername } from '../../utils';

const apiUrl = process.env.API_URL || 'http://localhost:1337';
const strapi = new Strapi(apiUrl);

class Signin extends React.Component {
  state = {
    username: '',
    password: '',
    loading: false
  };

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSubmit = async event => {
    const { username, password } = this.state;

    event.preventDefault();

    // TODO: Need better form management and validation
    if (this.isFormEmpty(this.state)) {
      toast.error('Please in all fields');
      return;
    }

    try {
      this.setState({ loading: true });
      const response = await strapi.login(username, password);
      console.log('loginResponse:', response);
      setToken(response.jwt);
      setUsername(response.user.username);
      setTimeout(() => {
        this.setState({ loading: false }, () => {
          this.redirectUser('/');
          toast.success(`Welcome back, ${username}!`, {
            autoClose: 2400
          });
        });
      }, 2000);
    } catch (error) {
      this.setState({ loading: false });
      toast.error(error.message);
    }
  };

  redirectUser = path => {
    this.props.history.push(path);
  };

  isFormEmpty = ({ username, password }) => !username || !password;

  render() {
    const { loading } = this.state;

    return (
      <div className="signin">
        <div className="content-wrapper">
          <form className="signin__form" onSubmit={this.handleSubmit}>
            <h2 className="signin__form-title">Login</h2>
            <input
              autoFocus
              type="text"
              name="username"
              id="username"
              placeholder="Username"
              onChange={this.handleChange}
              value={this.state.username}
            />
            <input
              type="password"
              name="password"
              id="password"
              placeholder="Password"
              onChange={this.handleChange}
              value={this.state.password}
            />
            <p className="signin__forgot-password">
              <Link to="/not-found">Forgot your password?</Link>
            </p>
            <button className="signin__form-submit" type="submit">
              Sign In <Loader show={loading} inline />
            </button>
            <p className="signin__return-to-store">
              or <Link to="/">Return to store</Link>
            </p>
          </form>
        </div>
      </div>
    );
  }
}

export default Signin;
