import React from 'react';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';

class ContactMap extends React.Component {
  state = {
    lat: 33.650842,
    lng: -117.744294,
    zoom: 16
  };

  componentDidMount() {
    const map = this.refs.map.leafletElement;
    map.once('focus', () => map.scrollWheelZoom.enable());
  }

  render() {
    const position = [this.state.lat, this.state.lng];
    return (
      <div className="contact-map">
        <Map
          center={position}
          zoom={this.state.zoom}
          className="contact-map__map-wrapper"
          ref="map"
        >
          <TileLayer
            attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Marker position={position}>
            <Popup className="contact-map__popup">
              <div className="contact-map__popup-content">
                <div className="contact-map__popup__copy-container">
                  <h2>OC Aqua Supply</h2>
                  <p>670 Spectrum Center Dr</p>
                  <p>Irvine, CA 92618</p>
                </div>
                <div className="contact-map__popup__image-container">
                  <img
                    className="contact-map__popup__image"
                    src="/icons/shrimp-logo.png"
                    alt="Shrimp logo"
                  />
                </div>
              </div>
            </Popup>
          </Marker>
        </Map>
      </div>
    );
  }
}

export default ContactMap;
