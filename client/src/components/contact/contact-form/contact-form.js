import React from 'react';
import Strapi from 'strapi-sdk-javascript/build/main';
import { toast } from 'react-toastify';

import Loader from '../../shared/loader';

const apiUrl = process.env.API_URL || 'http://localhost:1337';
const strapi = new Strapi(apiUrl);

const ContactFormEnum = Object.freeze({
  EMAIL: 'email',
  MESSAGE: 'message',
  MESSAGE_MAX_LENGTH: 300,
  NAME: 'name'
});

class ContactForm extends React.Component {
  state = {
    name: '',
    email: '',
    message: '',
    messageSending: false
  };

  handleChange = event => {
    event.persist();

    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleSendMessage = async event => {
    const { name, email, message } = this.state;
    const currentTime = new Date(Date.now());

    event.preventDefault();

    if (!this.isFormComplete(this.state))
      return toast.error('Form is incomplete.');

    this.setState({ messageSending: true });

    try {
      await strapi.request('POST', '/email', {
        data: {
          to: 'tester.senpai100@gmail.com',
          from: email,
          subject: `Customer Inquiry from ${name}: OCAqua ${currentTime}`,
          text: message,
          html: `<p>${message}</p>`
        }
      });
      toast('Message sent!', {
        onClose: this.setState({
          name: '',
          email: '',
          message: '',
          messageSending: false
        })
      });
    } catch (error) {
      this.setState({ messageSending: false });
      toast.error(error.message);
    }
  };

  isFormEmpty = ({ name, email, message }) => {
    return !(name || email || message);
  };
  isFormComplete = ({ name, email, message }) => {
    return name && email && message;
  };

  calculateMessageCharactersRemaining = message => {
    const messageCharactersRemaining =
      ContactFormEnum.MESSAGE_MAX_LENGTH - message.length;
    if (messageCharactersRemaining < 1) return 0;
    return messageCharactersRemaining;
  };

  render() {
    const { name, email, message, messageSending } = this.state;

    return (
      <div className="contact-form">
        <div className="contact-form__container">
          <h2 className="contact-form__title">Contact Us</h2>
          <p className="contact-form__copy">
            Got a question? We'd love to hear from you. Send us a message and
            we'll respond as soon as possible.
          </p>
          <form className="contact-form__wrapper">
            <fieldset disabled={messageSending}>
              <div className="contact-form__form-group">
                <label htmlFor="name">
                  Name<sup>*</sup>
                </label>
                <input
                  type="text"
                  id="name"
                  name="name"
                  value={name}
                  onChange={this.handleChange}
                />
              </div>

              <div className="contact-form__form-group">
                <label htmlFor="city">
                  Email<sup>*</sup>
                </label>
                <input
                  type="email"
                  id="email"
                  name="email"
                  value={email}
                  onChange={this.handleChange}
                />
              </div>

              <div className="contact-form__form-group">
                <label htmlFor="message">
                  Message<sup>*</sup>
                </label>
                <textarea
                  name="message"
                  id="message"
                  cols="30"
                  rows="10"
                  onChange={this.handleChange}
                  maxLength={ContactFormEnum.MESSAGE_MAX_LENGTH}
                  value={message}
                />
                <div className="contact-form__message-characters-remaining">
                  {ContactFormEnum.MESSAGE_MAX_LENGTH - message.length}
                </div>
              </div>

              <button
                className="contact-form__submit"
                disabled={this.isFormEmpty(this.state)}
                onClick={this.handleSendMessage}>
                Send Message <Loader show={messageSending} inline />
              </button>
            </fieldset>
          </form>
        </div>
      </div>
    );
  }
}

export default ContactForm;
