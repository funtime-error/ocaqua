import React from 'react';

import ContactForm from './contact-form/contact-form';
import ContactMap from './contact-map/contact-map';

const Contact = () => (
  <div className="contact">
    <div className="contact__content-wrapper content-wrapper">
      <ContactForm />
      <div className="contact__info">
        <div className="contact__info-container">
          <ContactMap />
          <div className="contact__info-details">
            <p>
              670 Spectrum Center Dr
              <br />
              Irvine, CA 92618
              <br />
              (909) 762-6065
            </p>
            <p>
              Monday: 9:00a - 7:00p
              <br />
              Tuesday: Closed
              <br />
              Wednesday - Saturday: 9:00a - 7:00p
              <br />
              Sunday: 11:00a - 6:00p
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Contact;
